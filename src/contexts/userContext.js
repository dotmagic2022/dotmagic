import React, { useState } from 'react'

export const UserContext = React.createContext(null)

export const UserProvider = ({children}) => {
    const [user, setUser] = useState()

    const stateValues = {
        user,
        setUser,
    }

    return (
        <UserContext.Provider value={stateValues}>
            {children}
        </UserContext.Provider>
    )
}
