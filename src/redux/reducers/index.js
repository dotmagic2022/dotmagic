const initialState = {
    content: null,
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SAVE_CONTENT':
            return {
                content: action.payload
            }
        default:
            return state
    }
}

export const saveContent = (content) => {
    return {
        type: 'SAVE_CONTENT',
        payload: content
    }
}
