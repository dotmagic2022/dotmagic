import React, { useContext } from 'react'
import {Link, Route, Routes, useLocation} from 'react-router-dom'

import st from './style.module.scss'

import { UserContext } from '../contexts/userContext'

import Login from '../components/google-auth/google-login-button'
import Logout from '../components/google-auth/google-logout-button'
import DefaultContent from '../components/content/default-content'
import HotContent from '../components/content/hot-content'
import Account from '../components/account'

import Cherry from './cherry.png'
import Meme from './meme.png'
import Heart from './heart.png'

const Layout = () => {
    const {user} = useContext(UserContext)

    const usePathname = () => {
        const location = useLocation();
        return location.pathname;
    }

    const isHome = usePathname() === '/'
    const isHot = usePathname() === '/hot'
    const isAccount = usePathname() === '/safe-place'

    return (
        <div className={st.layout}>
            <header className={st.header}>
                <div className={st.headerContainer}>
                    {user ? <Logout/> : <Login/>}
                    {(isHome || isAccount) && <Link to='/hot'><img src={Cherry} className={st.icon} alt='cherry'/></Link>}
                    {(isHot || isAccount) && <Link to='/'><img src={Meme} className={st.icon} alt='meme'/></Link>}
                    {user&& (isHot || isHome) && <Link to='/safe-place'><img src={Heart} className={st.icon} alt='heart'/></Link>}
                </div>
            </header>

            <Routes>
                <Route exact path='/' element={<DefaultContent className={st.content}/>}/>
                <Route path='/hot' element={<HotContent className={st.content}/>}/>
                <Route path='/safe-place' element={<Account/>}/>
            </Routes>

            <footer className={st.footer}>
                <div className={st.footerText}>© 2022 Dotmagic</div>
                <div className={st.footerText}>No think just watch</div>
            </footer>
        </div>
    )
}

export default Layout
