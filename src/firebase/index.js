import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAaEz0pvyD-mSry8X2YjNXJhuT8_nR8sAI",
    authDomain: "dotmagic-33b2f.firebaseapp.com",
    projectId: "dotmagic-33b2f",
    storageBucket: "dotmagic-33b2f.appspot.com",
    messagingSenderId: "1000998447827",
    appId: "1:1000998447827:web:6a85bc731780abe2a38226",
    measurementId: "G-7TBBPTT554"
}

firebase.initializeApp(firebaseConfig)

export default firebase


