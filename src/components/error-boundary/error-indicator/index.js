import React from 'react'
import st from './style.module.scss'
import { Link } from 'react-router-dom'

const ErrorIndicator = () => {
    return (
        <div className={st.errorWrapper}>
            <h1>Something unusual has happend</h1>
            <Link to='/' className={st.link}>Try back to memes</Link>
            <div className={st.text}>&nbsp;or reload app</div>
        </div>
    )
}

export default ErrorIndicator
