import React, {useContext, useState, useEffect} from "react";

import st from './style.module.scss'
import { UserContext } from '../../contexts/userContext'
import firebase from '../../firebase'
import { v4 as uuidv4 } from 'uuid'
import Loader from '../loader'

import Trash from './trash.png'

const Account = () => {
   const {user} = useContext(UserContext)
   const ref = firebase.firestore().collection('photos')

   const [data, setData] = useState([])
   const [loader, setLoader] = useState(true)
   const [trashVisible, setTrashVisible] = useState(false)

   let unsubscribe

   const getData = () => {
      unsubscribe = ref.onSnapshot((querySnapshot) => {
         const items = []
         querySnapshot.forEach((doc) => {
            items.push(doc.data())
         })
         setData(items)
      })
   }

   useEffect(() => {
      getData()
      setTimeout(() => setTrashVisible(true), 3000)
      setTimeout(() => setLoader(false), 500)
      const cleanup = () => {
         return unsubscribe()
      }
   }, [])

   const deleteData = (docx) => {
      ref.doc(docx.id).delete()
          .catch((err) => {console.error(err)})
   }

   return (
       <div className={st.account}>
          <h1>Favourites</h1>
          {loader && <Loader />}
          {loader === false && (data.sort((a,b) => b.date - a.date).map((photo) => (
              user && user.googleId === photo.userId && (
                  <div className={st.photoWrapper} key={photo.id}>
                     <img className={st.photo} src={photo.url}/>
                     {trashVisible && <img src={Trash} className={st.deleteIcon} onClick={() => deleteData(photo)} />}
                  </div>
              )
          )))}
          {!user && <div>Login to save photos</div>}
       </div>
   )
}

export default Account
