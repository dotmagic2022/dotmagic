import React, { useContext } from 'react'
import { GoogleLogin } from 'react-google-login'
import { refreshToken } from '../utils'
import AuthButton from '../auth-button'
import { UserContext } from "../../../contexts/userContext";

const clientID = '64283102605-b5v2d4k4kk9fkacedtl8uv1635phkgbe.apps.googleusercontent.com'

const Login = () => {
    const {setUser} = useContext(UserContext)

    const success = (res) => {
        console.log('[Login success]')
        refreshToken(res)
        setUser(res.profileObj)
    }

    const failure = (res) => {
        console.log('[Login failed]', res)
    }

    return (
        <div>
            <GoogleLogin
                clientId={clientID}
                render={renderProps => (
                    <div onClick={renderProps.onClick} disabled={renderProps.disabled}>
                        <AuthButton type={'Login'}/>
                    </div>
                )}
                buttonText='Login'
                onSuccess={success}
                onFailure={failure}
                cookiePolicy={'single_host_origin'}
                isSignedIn={true}
            />
        </div>
    )
}

export default Login

