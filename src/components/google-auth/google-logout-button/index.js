import React, {useContext} from 'react'
import { GoogleLogout } from 'react-google-login'
import AuthButton from '../auth-button';
import { UserContext } from "../../../contexts/userContext";

const clientID = '64283102605-b5v2d4k4kk9fkacedtl8uv1635phkgbe.apps.googleusercontent.com'

const Logout = () => {
    const {setUser} = useContext(UserContext)

    const success = () => {
        console.log('[Logout success]')
        setUser(null)
    }

    return (
        <div>
            <GoogleLogout
                clientId={clientID}
                render={renderProps => (
                    <div onClick={renderProps.onClick} disabled={renderProps.disabled}>
                        <AuthButton type={'Logout'}/>
                    </div>
                )}
                buttonText='Logout'
                onLogoutSuccess={success}
            />
        </div>
    )
}

export default Logout

