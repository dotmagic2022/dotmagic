export const refreshToken = (res) => {
    let refreshTime = res.tokenObj.expires_in * 1000
    const refresh = async () => {
        const newRes = await res.reloadAuthResponse()
        refreshTime = newRes.expires_in * 1000
        setTimeout(refresh, refreshTime)
    }
    setTimeout(refresh, refreshTime)
}
