import React from 'react'
import PropTypes from 'prop-types'
import st from './style.module.scss'

const AuthButton = ({type}) => {
    return (
        <button className={st.button}>{type}</button>
    )
}

AuthButton.propTypes = {
    type: PropTypes.string,
}

export default AuthButton
