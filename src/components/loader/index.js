import React from 'react'
import st from './style.module.scss'
import Spinner from './spinner.gif'

const Loader = () => {
    return <img className={st.spinner} alt='spinner' src={Spinner} />
}

export default Loader
