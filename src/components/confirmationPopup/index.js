import React, { useState } from 'react'
import st from './style.module.scss'
import PropTypes from 'prop-types'

const ConfirmationPopup = ({ setIsAdult, setLoader }) => {
    return (
        <div className={st.popup}>
            <div className={st.question}>Are you at least 18 years old ?</div>
            <div className={st.answersWrrapper}>
                <button className={st.answer} onClick={() => {setIsAdult(true); setTimeout(() => setLoader(false), 500);}} >Yes</button>
                <button className={st.answer} onClick={() => setIsAdult(false)}>No</button>
            </div>
        </div>
    )
}

ConfirmationPopup.propTypes = {
    setIsAdult: PropTypes.func,
    setLoader: PropTypes.func,
}

export default ConfirmationPopup
