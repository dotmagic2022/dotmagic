import React, {useContext, useEffect, useState} from 'react'
import { connect } from 'react-redux'
import { content, saveContent } from '../../../redux/reducers'
import { request } from '../../../api'
import st from './style.module.scss'
import firebase from '../../../firebase'
import { v4 as uuidv4 } from 'uuid'
import {UserContext} from '../../../contexts/userContext'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Loader from '../../loader'
import Heart from './heart.png'

const mapStateToProps = ({ content }) => {
    return { content }
}

const mapDispatchToProps = {
    saveContent
}

const DefaultContent = ({ content, saveContent }) => {
    const {user} = useContext(UserContext)
    const ref = firebase.firestore().collection('photos')
    const memeUrl = 'https://meme-api.herokuapp.com/gimme'

    const [favourite, setFavourite] = useState(false)
    const [loader, setLoader] = useState(true)

    useEffect(() => {
        request(memeUrl)
            .then((data) => {
                saveContent(data)
                setLoader(false)
            })
            .catch((err) => {
                console.error(err)
            })
    }, [])

    const fetchData = () => {
        setLoader(true)
        request(memeUrl)
            .then((data) => {
                saveContent(data)
                setFavourite(false)
                setLoader(false)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const saveData = (newDataObj) => {
        ref.doc(newDataObj.id).set(newDataObj).catch((err) => {
            console.error(err)
        })
    }

    return (
        <div className={st.container}>
            {loader && <Loader/>}
            {loader === false &&
                <>
                    <div className={st.photoWrapper}>
                        {content && content.url && <img className={st.photo} src={content.url} alt='meme'/>}
                        {user && <img src={Heart} className={favourite ? cn(st.heart, st.heartFavourite) : st.heart}
                             onClick={() => {saveData({userId: user.googleId, url: content.url, date: Date.now(), id: uuidv4()}); setFavourite(true)}} />
                        }
                    </div>
                    <button className={st.buttonMore} onClick={fetchData}>More</button>
                </>
            }
        </div>
    )
}

DefaultContent.propTypes = {
    content: PropTypes.object,
    saveContent: PropTypes.func,
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultContent)
