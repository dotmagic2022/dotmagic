import React, {useContext, useEffect, useState} from 'react'
import { Navigate } from 'react-router-dom';
import { connect } from 'react-redux'
import { content, saveContent } from '../../../redux/reducers'
import { request } from '../../../api'
import st from './style.module.scss'
import ConfirmationPopup from '../../confirmationPopup'
import {UserContext} from '../../../contexts/userContext'
import firebase from '../../../firebase'
import { v4 as uuidv4 } from 'uuid'
import cn from 'classnames'
import Loader from '../../loader'
import Heart from './heart.png'
import PropTypes from 'prop-types';

const mapStateToProps = ({ content }) => {
    return { content }
}

const mapDispatchToProps = {
    saveContent
}

const HotContent = ({ content, saveContent }) => {
    const [randomUrl, setRandomUrl] = useState()
    const [isAdult, setIsAdult] = useState(null)
    const [favourite, setFavourite] = useState(false)
    const [loader, setLoader] = useState(true)

    const {user} = useContext(UserContext)
    const ref = firebase.firestore().collection('photos')
    const baseUrl = 'https://meme-api.herokuapp.com/gimme'

    const girlsUrl = `${baseUrl}/gonewild`
    const catsUrl = `${baseUrl}/pussy`
    const cocksUrl = `${baseUrl}/OnlyIfShesPackin`

    const urls = [girlsUrl, catsUrl, cocksUrl]

    useEffect(() => {
        request(catsUrl)
            .then((data) => {
                saveContent(data)
            })
            .catch((err) => {
                console.error(err)
            })
    }, [])

    useEffect(() => {
        setRandomUrl(urls[Math.floor(Math.random()*urls.length)])
    }, [content])

    const fetchData = () => {
        setLoader(true)
        request(randomUrl)
            .then((data) => {
                saveContent(data)
                setFavourite(false)
                setLoader(false)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const saveData = (newDataObj) => {
        ref.doc(newDataObj.id).set(newDataObj).catch((err) => {
            console.error(err)
        })
    }

    if (isAdult === null) {
        return <ConfirmationPopup setIsAdult={setIsAdult} setLoader={setLoader}/>
    }
    if (isAdult === false) {
        return <Navigate to='/'/>
    }
    if (isAdult === true) {
        return (
            <div className={st.container}>
                {loader && <Loader/>}
                {loader === false &&
                <>
                    <div className={st.photoWrapper}>
                        {content && content.url && <img className={st.photo} src={content.url} alt='hot'/>}
                        {user && <img src={Heart} alt='heart' className={favourite ? cn(st.heart, st.heartFavourite) : st.heart}
                             onClick={() => {
                                 saveData({userId: user.googleId, url: content.url, date: Date.now(), id: uuidv4()});
                                 setFavourite(true)
                             }}/>
                        }
                    </div>
                    <button className={st.buttonMore} onClick={fetchData}>More</button>
                </>
                }
            </div>
        )
    }
}

HotContent.propTypes = {
    content: PropTypes.object,
    saveContent: PropTypes.func,
}

export default connect(mapStateToProps, mapDispatchToProps)(HotContent)
