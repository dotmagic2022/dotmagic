import React, { useEffect } from 'react'
import Layout from './layout'
import { UserProvider } from './contexts/userContext'
import { Provider } from 'react-redux'
import ErrorBoundary from './components/error-boundary'
import store from './redux/store'
import { BrowserRouter as Router } from 'react-router-dom'

function App() {
    return (
        <Provider store={store}>
            <UserProvider>
                <Router>
                    <ErrorBoundary>
                        <Layout/>
                    </ErrorBoundary>
                </Router>
            </UserProvider>
        </Provider>
    )
}

export default App
